#!/bin/bash

echo "Creating services on cluster..."

kubectl apply -f ./services.yml

echo "Creating deployments..."

kubectl apply -f ./deployment1.0.yml
